<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m200217_075637_create_product_table extends Migration
{
    public $table               = 'product';
    public $catalogTable        = 'catalog';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'category_id'   => $this->integer()->notNull(),
            'status'        => $this->integer()->defaultValue(1)->notNull(),
            'name'          => $this->string(255)->notNull(),
            'code'          => $this->string(255)->null(),
            'price'         => $this->integer()->null(),
            'slug'          => $this->text()->notNull(),
            'content'       => $this->text()->null(),
            'image'         => $this->string(255)->null(),
            'sort'          => $this->integer()->null(),
            'metaName'      => $this->string(255)->null(),
            'metaDesc'      => $this->text()->null(),
            'metaKey'       => $this->text()->null(),
            'create_at'     => $this->timestamp()->null(),
        ], $tableOptions);

        $this->addForeignKey("fk_{$this->table}_{$this->catalogTable}", "{{{$this->table}}}", 'category_id', "{{{$this->catalogTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->catalogTable}", "{{{$this->catalogTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}

<?php

namespace console\controllers;

use common\modules\user\models\BaseUser;

/**
 * Class RbacController
 * @package console\controllers
 */
class RbacController extends \common\modules\user\controllers\RbacController
{
    public $defaultAction = 'init';

    /**
     * @inheritdoc
     */
    public function actionInit()
    {
        parent::actionInit();

        /** Управление о компании */
        $createAbout              = $this->authManager->createPermission('createAbout');
        $createAbout->description = 'Создание о компании';
        $viewAbout                = $this->authManager->createPermission('viewAbout');
        $viewAbout->description   = 'Просмотр о компании';
        $indexAbout               = $this->authManager->createPermission('indexAbout');
        $indexAbout->description  = 'Листинг о компании';
        $updateAbout              = $this->authManager->createPermission('updateAbout');
        $updateAbout->description = 'Изменение о компании';
        $deleteAbout              = $this->authManager->createPermission('deleteAbout');
        $deleteAbout->description = 'Удаление о компании';

        /** Управление брендами */
        $createBrand              = $this->authManager->createPermission('createBrand');
        $createBrand->description = 'Создание бренда';
        $viewBrand                = $this->authManager->createPermission('viewBrand');
        $viewBrand->description   = 'Просмотр бренда';
        $indexBrand               = $this->authManager->createPermission('indexBrand');
        $indexBrand->description  = 'Листинг бренда';
        $updateBrand              = $this->authManager->createPermission('updateBrand');
        $updateBrand->description = 'Изменение бренда';
        $deleteBrand              = $this->authManager->createPermission('deleteBrand');
        $deleteBrand->description = 'Удаление бренда';

        /** Управление контактами */
        $createContact              = $this->authManager->createPermission('createContact');
        $createContact->description = 'Создание контакта';
        $viewContact                = $this->authManager->createPermission('viewContact');
        $viewContact->description   = 'Просмотр контакта';
        $indexContact               = $this->authManager->createPermission('indexContact');
        $indexContact->description  = 'Листинг контакта';
        $updateContact              = $this->authManager->createPermission('updateContact');
        $updateContact->description = 'Изменение контакта';
        $deleteContact              = $this->authManager->createPermission('deleteContact');
        $deleteContact->description = 'Удаление контакта';

        /** Управление страницы оплата и доставка */
        $createDelivery              = $this->authManager->createPermission('createDelivery');
        $createDelivery->description = 'Создание страницы оплата и доставка';
        $viewDelivery                = $this->authManager->createPermission('viewDelivery');
        $viewDelivery->description   = 'Просмотр страницы оплата и доставка';
        $indexDelivery               = $this->authManager->createPermission('indexDelivery');
        $indexDelivery->description  = 'Листинг страницы оплата и доставка';
        $updateDelivery              = $this->authManager->createPermission('updateDelivery');
        $updateDelivery->description = 'Изменение страницы оплата и доставка';
        $deleteDelivery              = $this->authManager->createPermission('deleteDelivery');
        $deleteDelivery->description = 'Удаление страницы оплата и доставка';

        /** Управление новости */
        $createNews              = $this->authManager->createPermission('createNews');
        $createNews->description = 'Создание новости';
        $viewNews                = $this->authManager->createPermission('viewNews');
        $viewNews->description   = 'Просмотр новости';
        $indexNews               = $this->authManager->createPermission('indexNews');
        $indexNews->description  = 'Листинг новости';
        $updateNews              = $this->authManager->createPermission('updateNews');
        $updateNews->description = 'Изменение новости';
        $deleteNews              = $this->authManager->createPermission('deleteNews');
        $deleteNews->description = 'Удаление новости';

        /** Управление реквизитами */
        $createRequisites              = $this->authManager->createPermission('createRequisites');
        $createRequisites->description = 'Создание реквизита';
        $viewRequisites                = $this->authManager->createPermission('viewRequisites');
        $viewRequisites->description   = 'Просмотр реквизита';
        $indexRequisites               = $this->authManager->createPermission('indexRequisites');
        $indexRequisites->description  = 'Листинг реквизита';
        $updateRequisites              = $this->authManager->createPermission('updateRequisites');
        $updateRequisites->description = 'Изменение реквизита';
        $deleteRequisites              = $this->authManager->createPermission('deleteRequisites');
        $deleteRequisites->description = 'Удаление реквизита';

        /** Управление страницы жалобы и предложение */
        $createSuggestion              = $this->authManager->createPermission('createSuggestion');
        $createSuggestion->description = 'Создание страницы жалобы и предложение';
        $viewSuggestion                = $this->authManager->createPermission('viewSuggestion');
        $viewSuggestion->description   = 'Просмотр страницы жалобы и предложение';
        $indexSuggestion               = $this->authManager->createPermission('indexSuggestion');
        $indexSuggestion->description  = 'Листинг страницы жалобы и предложение';
        $updateSuggestion              = $this->authManager->createPermission('updateSuggestion');
        $updateSuggestion->description = 'Изменение страницы жалобы и предложение';
        $deleteSuggestion              = $this->authManager->createPermission('deleteSuggestion');
        $deleteSuggestion->description = 'Удаление страницы жалобы и предложение';

        /** Управление бонусной системой */
        $createBonusSystem              = $this->authManager->createPermission('createBonusSystem');
        $createBonusSystem->description = 'Создание бонусной системой';
        $viewBonusSystem                = $this->authManager->createPermission('viewBonusSystem');
        $viewBonusSystem->description   = 'Просмотр бонусной системой';
        $indexBonusSystem               = $this->authManager->createPermission('indexBonusSystem');
        $indexBonusSystem->description  = 'Листинг бонусной системой';
        $updateBonusSystem              = $this->authManager->createPermission('updateBonusSystem');
        $updateBonusSystem->description = 'Изменение бонусной системой';
        $deleteBonusSystem              = $this->authManager->createPermission('deleteBonusSystem');
        $deleteBonusSystem->description = 'Удаление бонусной системой';

        /** Управление документами */
        $createDocuments              = $this->authManager->createPermission('createDocuments');
        $createDocuments->description = 'Создание документа';
        $viewDocuments                = $this->authManager->createPermission('viewDocuments');
        $viewDocuments->description   = 'Просмотр документа';
        $indexDocuments               = $this->authManager->createPermission('indexDocuments');
        $indexDocuments->description  = 'Листинг документа';
        $updateDocuments              = $this->authManager->createPermission('updateDocuments');
        $updateDocuments->description = 'Изменение документа';
        $deleteDocuments              = $this->authManager->createPermission('deleteDocuments');
        $deleteDocuments->description = 'Удаление документа';

        /** Управление страницы гарантия и возврат */
        $createGuarantee              = $this->authManager->createPermission('createGuarantee');
        $createGuarantee->description = 'Создание страницы гарантия и возврат';
        $viewGuarantee                = $this->authManager->createPermission('viewGuarantee');
        $viewGuarantee->description   = 'Просмотр страницы гарантия и возврат';
        $indexGuarantee               = $this->authManager->createPermission('indexGuarantee');
        $indexGuarantee->description  = 'Листинг страницы гарантия и возврат';
        $updateGuarantee              = $this->authManager->createPermission('updateGuarantee');
        $updateGuarantee->description = 'Изменение страницы гарантия и возврат';
        $deleteGuarantee              = $this->authManager->createPermission('deleteGuarantee');
        $deleteGuarantee->description = 'Удаление страницы гарантия и возврат';

        /** Управление электронной почтой для обратной связи */
        $createEmailForRequest              = $this->authManager->createPermission('createEmailForRequest');
        $createEmailForRequest->description = 'Создание электронной почтой для обратной связи';
        $viewEmailForRequest                = $this->authManager->createPermission('viewEmailForRequest');
        $viewEmailForRequest->description   = 'Просмотр электронной почтой для обратной связи';
        $indexEmailForRequest               = $this->authManager->createPermission('indexEmailForRequest');
        $indexEmailForRequest->description  = 'Листинг электронной почтой для обратной связи';
        $updateEmailForRequest              = $this->authManager->createPermission('updateEmailForRequest');
        $updateEmailForRequest->description = 'Изменение электронной почтой для обратной связи';
        $deleteEmailForRequest              = $this->authManager->createPermission('deleteEmailForRequest');
        $deleteEmailForRequest->description = 'Удаление электронной почтой для обратной связи';

        /** Управление обратной связи */
        $createRequestSuggestion              = $this->authManager->createPermission('createRequestSuggestion');
        $createRequestSuggestion->description = 'Создание обратной связи';
        $viewRequestSuggestion                = $this->authManager->createPermission('viewRequestSuggestion');
        $viewRequestSuggestion->description   = 'Просмотр обратной связи';
        $indexRequestSuggestion               = $this->authManager->createPermission('indexRequestSuggestion');
        $indexRequestSuggestion->description  = 'Листинг обратной связи';
        $updateRequestSuggestion              = $this->authManager->createPermission('updateRequestSuggestion');
        $updateRequestSuggestion->description = 'Изменение обратной связи';
        $deleteRequestSuggestion              = $this->authManager->createPermission('deleteRequestSuggestion');
        $deleteRequestSuggestion->description = 'Удаление обратной связи';

        /** Управление заказами */
        $createOrders              = $this->authManager->createPermission('createOrders');
        $createOrders->description = 'Создание заказами';
        $viewOrders                = $this->authManager->createPermission('viewOrders');
        $viewOrders->description   = 'Просмотр заказами';
        $indexOrders               = $this->authManager->createPermission('indexOrders');
        $indexOrders->description  = 'Листинг заказами';
        $updateOrders              = $this->authManager->createPermission('updateOrders');
        $updateOrders->description = 'Изменение заказами';
        $deleteOrders              = $this->authManager->createPermission('deleteOrders');
        $deleteOrders->description = 'Удаление заказами';

        /** Управление аттрибутами фильтров */
        $createFilterAttr              = $this->authManager->createPermission('createFilterAttr');
        $createFilterAttr->description = 'Создание аттрибутами фильтров';
        $viewFilterAttr                = $this->authManager->createPermission('viewFilterAttr');
        $viewFilterAttr->description   = 'Просмотр аттрибутами фильтров';
        $indexFilterAttr               = $this->authManager->createPermission('indexFilterAttr');
        $indexFilterAttr->description  = 'Листинг аттрибутами фильтров';
        $updateFilterAttr              = $this->authManager->createPermission('updateFilterAttr');
        $updateFilterAttr->description = 'Изменение аттрибутами фильтров';
        $deleteFilterAttr              = $this->authManager->createPermission('deleteFilterAttr');
        $deleteFilterAttr->description = 'Удаление аттрибутами фильтров';

        /** Управление сущностями фильтра */
        $createFilterEntity              = $this->authManager->createPermission('createFilterEntity');
        $createFilterEntity->description = 'Создание сущностями фильтра';
        $viewFilterEntity                = $this->authManager->createPermission('viewFilterEntity');
        $viewFilterEntity->description   = 'Просмотр сущностями фильтра';
        $indexFilterEntity               = $this->authManager->createPermission('indexFilterEntity');
        $indexFilterEntity->description  = 'Листинг сущностями фильтра';
        $updateFilterEntity              = $this->authManager->createPermission('updateFilterEntity');
        $updateFilterEntity->description = 'Изменение сущностями фильтра';
        $deleteFilterEntity              = $this->authManager->createPermission('deleteFilterEntity');
        $deleteFilterEntity->description = 'Удаление сущностями фильтра';

        /** Управление значение фильтров */
        $createFilterValue              = $this->authManager->createPermission('createFilterValue');
        $createFilterValue->description = 'Создание значение фильтров';
        $viewFilterValue                = $this->authManager->createPermission('viewFilterValue');
        $viewFilterValue->description   = 'Просмотр значение фильтров';
        $indexFilterValue               = $this->authManager->createPermission('indexFilterValue');
        $indexFilterValue->description  = 'Листинг значение фильтров';
        $updateFilterValue              = $this->authManager->createPermission('updateFilterValue');
        $updateFilterValue->description = 'Изменение значение фильтров';
        $deleteFilterValue              = $this->authManager->createPermission('deleteFilterValue');
        $deleteFilterValue->description = 'Удаление значение фильтров';

        /** Управление продуктами */
        $createProduct              = $this->authManager->createPermission('createProduct');
        $createProduct->description = 'Создание продуктами';
        $viewProduct                = $this->authManager->createPermission('viewProduct');
        $viewProduct->description   = 'Просмотр продуктами';
        $indexProduct               = $this->authManager->createPermission('indexProduct');
        $indexProduct->description  = 'Листинг продуктами';
        $updateProduct              = $this->authManager->createPermission('updateProduct');
        $updateProduct->description = 'Изменение продуктами';
        $deleteProduct              = $this->authManager->createPermission('deleteProduct');
        $deleteProduct->description = 'Удаление продуктами';

        /** Управление каталогом */
        $createCatalog              = $this->authManager->createPermission('createCatalog');
        $createCatalog->description = 'Создание каталогом';
        $viewCatalog                = $this->authManager->createPermission('viewCatalog');
        $viewCatalog->description   = 'Просмотр каталогом';
        $indexCatalog               = $this->authManager->createPermission('indexCatalog');
        $indexCatalog->description  = 'Листинг каталогом';
        $updateCatalog              = $this->authManager->createPermission('updateCatalog');
        $updateCatalog->description = 'Изменение каталогом';
        $deleteCatalog              = $this->authManager->createPermission('deleteCatalog');
        $deleteCatalog->description = 'Удаление каталогом';

        echo 'New permissions has been created.'.PHP_EOL;

        // ------------------------------------------------------------------------------

        /** Регистрация правил */

        $this->authManager->add($createAbout);
        $this->authManager->add($viewAbout);
        $this->authManager->add($indexAbout);
        $this->authManager->add($updateAbout);
        $this->authManager->add($deleteAbout);

        $this->authManager->add($createBrand);
        $this->authManager->add($viewBrand);
        $this->authManager->add($indexBrand);
        $this->authManager->add($updateBrand);
        $this->authManager->add($deleteBrand);

        $this->authManager->add($createContact);
        $this->authManager->add($viewContact);
        $this->authManager->add($indexContact);
        $this->authManager->add($updateContact);
        $this->authManager->add($deleteContact);

        $this->authManager->add($createDelivery);
        $this->authManager->add($viewDelivery);
        $this->authManager->add($indexDelivery);
        $this->authManager->add($updateDelivery);
        $this->authManager->add($deleteDelivery);

        $this->authManager->add($createNews);
        $this->authManager->add($viewNews);
        $this->authManager->add($indexNews);
        $this->authManager->add($updateNews);
        $this->authManager->add($deleteNews);

        $this->authManager->add($createRequisites);
        $this->authManager->add($viewRequisites);
        $this->authManager->add($indexRequisites);
        $this->authManager->add($updateRequisites);
        $this->authManager->add($deleteRequisites);

        $this->authManager->add($createSuggestion);
        $this->authManager->add($viewSuggestion);
        $this->authManager->add($indexSuggestion);
        $this->authManager->add($updateSuggestion);
        $this->authManager->add($deleteSuggestion);

        $this->authManager->add($createBonusSystem);
        $this->authManager->add($viewBonusSystem);
        $this->authManager->add($indexBonusSystem);
        $this->authManager->add($updateBonusSystem);
        $this->authManager->add($deleteBonusSystem);

        $this->authManager->add($createDocuments);
        $this->authManager->add($viewDocuments);
        $this->authManager->add($indexDocuments);
        $this->authManager->add($updateDocuments);
        $this->authManager->add($deleteDocuments);

        $this->authManager->add($createGuarantee);
        $this->authManager->add($viewGuarantee);
        $this->authManager->add($indexGuarantee);
        $this->authManager->add($updateGuarantee);
        $this->authManager->add($deleteGuarantee);

        $this->authManager->add($createEmailForRequest);
        $this->authManager->add($viewEmailForRequest);
        $this->authManager->add($indexEmailForRequest);
        $this->authManager->add($updateEmailForRequest);
        $this->authManager->add($deleteEmailForRequest);

        $this->authManager->add($createRequestSuggestion);
        $this->authManager->add($viewRequestSuggestion);
        $this->authManager->add($indexRequestSuggestion);
        $this->authManager->add($updateRequestSuggestion);
        $this->authManager->add($deleteRequestSuggestion);

        $this->authManager->add($createOrders);
        $this->authManager->add($viewOrders);
        $this->authManager->add($indexOrders);
        $this->authManager->add($updateOrders);
        $this->authManager->add($deleteOrders);

        $this->authManager->add($createFilterAttr);
        $this->authManager->add($viewFilterAttr);
        $this->authManager->add($indexFilterAttr);
        $this->authManager->add($updateFilterAttr);
        $this->authManager->add($deleteFilterAttr);

        $this->authManager->add($createFilterEntity);
        $this->authManager->add($viewFilterEntity);
        $this->authManager->add($indexFilterEntity);
        $this->authManager->add($updateFilterEntity);
        $this->authManager->add($deleteFilterEntity);

        $this->authManager->add($createFilterValue);
        $this->authManager->add($viewFilterValue);
        $this->authManager->add($indexFilterValue);
        $this->authManager->add($updateFilterValue);
        $this->authManager->add($deleteFilterValue);

        $this->authManager->add($createProduct);
        $this->authManager->add($viewProduct);
        $this->authManager->add($indexProduct);
        $this->authManager->add($updateProduct);
        $this->authManager->add($deleteProduct);

        $this->authManager->add($createCatalog);
        $this->authManager->add($viewCatalog);
        $this->authManager->add($indexCatalog);
        $this->authManager->add($updateCatalog);
        $this->authManager->add($deleteCatalog);

        echo 'New permissions has been added.'.PHP_EOL;

        // ------------------------------------------------------------------------------

        /** Добавление правил оператору */

        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $createOrders);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $viewOrders);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $indexOrders);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $updateOrders);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $deleteOrders);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $createRequestSuggestion);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $viewRequestSuggestion);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $indexRequestSuggestion);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $updateRequestSuggestion);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_OPERATOR], $deleteRequestSuggestion);

        /** Добавление правил менеджеру */

        // от оператора
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $this->roles[BaseUser::ROLE_OPERATOR]);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createAbout);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewAbout);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexAbout);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateAbout);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteAbout);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createBrand);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewBrand);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexBrand);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateBrand);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteBrand);

//        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createContact); // only for developer
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewContact);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexContact);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateContact);
//        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteContact); // only for developer

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createDelivery);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewDelivery);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexDelivery);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateDelivery);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteDelivery);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createNews);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewNews);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexNews);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateNews);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteNews);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createRequisites);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewRequisites);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexRequisites);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateRequisites);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteRequisites);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createSuggestion);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewSuggestion);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexSuggestion);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateSuggestion);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteSuggestion);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createBonusSystem);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewBonusSystem);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexBonusSystem);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateBonusSystem);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteBonusSystem);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createDocuments);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewDocuments);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexDocuments);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateDocuments);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteDocuments);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createGuarantee);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewGuarantee);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexGuarantee);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateGuarantee);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteGuarantee);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createEmailForRequest);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewEmailForRequest);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexEmailForRequest);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateEmailForRequest);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteEmailForRequest);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createFilterAttr);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewFilterAttr);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexFilterAttr);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateFilterAttr);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteFilterAttr);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createFilterEntity);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewFilterEntity);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexFilterEntity);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateFilterEntity);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteFilterEntity);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createFilterValue);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewFilterValue);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexFilterValue);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateFilterValue);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteFilterValue);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createProduct);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewProduct);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexProduct);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateProduct);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteProduct);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $createCatalog);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $viewCatalog);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $indexCatalog);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $updateCatalog);
        $this->authManager->addChild($this->roles[BaseUser::ROLE_MANAGER], $deleteCatalog);

        /** Добавление правил админу */

        // от менеджера
        $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $this->roles[BaseUser::ROLE_MANAGER]);

        /** Добавление правил разработчику */

        // от админа
        $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $this->roles[BaseUser::ROLE_ADMIN]);

        $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $createContact); // only for developer
        $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $deleteContact); // only for developer

        echo 'All child permissions has been added.'.PHP_EOL;

        echo 'RBAC generate Complete.'.PHP_EOL;
    }
}

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'По сайту', 'options' => ['class' => 'header']],
                    ['label' => 'Каталог', 'icon' => 'bookmark-o', 'url' => ['/catalog']],
                    ['label' => 'Продукт', 'icon' => 'bookmark-o', 'url' => ['/product']],
                ],
            ]
        ) ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Основное', 'options' => ['class' => 'header']],
//                    ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/source-message/']],
//                    ['label' => 'Пользователи', 'icon' => 'user-circle-o', 'url' => ['/users']],
//                    ['label' => 'Информация о клиентах', 'icon' => 'user-circle-o', 'url' => ['/client']],
//                    ['label' => 'Язык', 'icon' => 'language', 'url' => ['/language']],
//                    ['label' => 'Баннер', 'icon' => 'columns', 'url' => ['/banner']],
//                    ['label' => 'Баннер мобильной', 'icon' => 'columns', 'url' => ['/banner-mobile']],
//                    ['label' => 'Логотип', 'icon' => 'image', 'url' => ['/logo']],
//                    ['label' => 'Документы', 'icon' => 'image', 'url' => ['/documents']],
//                    ['label' => 'Бонусы', 'icon' => 'ship', 'url' => ['/bonus-system']],
//                    ['label' => 'Элекронная почта', 'icon' => 'ship', 'url' => ['/email-for-request']],

                ],
            ]
        ) ?>

    </section>

</aside>

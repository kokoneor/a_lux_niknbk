<?php
namespace frontend\models;

use common\modules\user\models\BaseUser;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $email;
    public $password;

    /**
     * Labels for fields
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email'           => 'Электронная почта',
            'password'        => 'Пароль',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User',
                'message' => Yii::t('main', 'Этот адрес электронной почты уже занят.')],

            ['password', 'required'],
            ['password', 'string', 'min' => 8],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool|\common\modules\user\models\User
     */
    public function signup()
    {
        if ($this->validate()) {
            $user           = new User();
            $user->email    = $this->email;
            $user->role     = BaseUser::ROLE_USER;
            $user->status   = BaseUser::STATUS_ACTIVE;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateToken();
            $user->save();
            $this->sendEmail($user);

            return $user;
        }

        return false;
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'NSCOM robot'])
            ->setTo($this->email)
            ->setSubject('Вы зарегистрировались на сайте NSCOM')
            ->send();
    }
}

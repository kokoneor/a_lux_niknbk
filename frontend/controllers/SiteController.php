<?php
namespace frontend\controllers;

use common\models\Banner;
use common\models\BannerMobile;
use common\models\BannerMobileProduct;
use common\models\BannerProduct;
use common\models\Brand;
use common\models\Catalog;
use common\models\FavoritesProduct;
use common\models\Product;
use frontend\models\Basket;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index', [

        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}

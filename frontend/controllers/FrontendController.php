<?php

namespace frontend\controllers;

use common\models\Catalog;
use common\models\City;
use common\models\Contact;
use common\models\Documents;
use common\models\Language;
use common\models\Logo;
use common\models\Menu;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;

class FrontendController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function init()
    {

    }

    protected function setMeta($title = null, $description = null, $keywords = null)
    {
        $this->view->title      =   $title;
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
    }
}
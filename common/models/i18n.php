<?php

namespace common\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

/**
 * Class i18n
 * @package common\models
 */

class i18n
{
    /**
     * @return array|mixed
     * @throws InvalidConfigException|InvalidParamException
     */

    public static function locales()
    {
        $config = require  Yii::getAlias('@common'). '/message/config.php';

        if(false === $locales = ArrayHelper::getValue($config, 'languages', false)){
            throw new InvalidParamException("Need to set available locales in 'config.php' file: ['ru','en'] or ['ru' => 'Ru', 'en' => 'En']");
        }

        if(array_values($locales) !== $locales){
            $locales = array_keys($locales);
        }

        if(!in_array(Yii::$app->language, $locales)){
            throw new InvalidConfigException(sprintf("Only '%s' languages allowed in 'config.php' of your app, not '%s'.", implode(', ', $locales), Yii::$app->language));
        }

        return $locales;
    }
}